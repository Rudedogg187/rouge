const https = require("https");
const fetch = require("node-fetch");
const nodemailer = require("nodemailer");
const fs = require("fs");
const express = require("express");
const cors = require("cors");

const app = express();


const log_file = "./log.json";
const refresh_minutes = 1;

const credentials = {
  key: fs.readFileSync("key.pem"),
  cert: fs.readFileSync("cert.pem"),
};

app.use(cors());

app.get("/log", (req, res) => {
  let log = JSON.parse(readLog());
  res.json(log)
  
})

const server = https.createServer(credentials, app);

server.listen(8000, () => console.log("**** Live on Port: 8000 ****\n\n"));

const delay = (ms) => {
  return new Promise(res => setTimeout(res, ms));
}

const now = () => {
  let now = new Date();
  let dt = now.toLocaleString("en-CA", {hour12: false}).replace(", ", "T");
  return dt;
}

const url = `https://www.roguefitness.com`;
const path_lst = [
    {
      path: `black-concept-2-model-d-rower-pm5`,
      product: {
	type: "Rower",
        color: "Black",
	model: "D", 
      }

    }, {
      path: `gray-concept-2-model-d-rower-pm5`,
      product: {
	type: "Rower",
        color: "Gray",
	model: "D", 
      }

    }, {
      path: `concept2-model-e-rower-pm5-black`,
      product: {
	type: "Rower",
        color: "Black",
	model: "E", 
      }

    }, { 
      path: `concept2-model-e-rower-pm5`, 
      product: {
	type: "Rower",
        color: "Gray",
	model: "E", 
      }

    }, /* { 
      path: `vinyl-rower-mat`,
      product: {
	type: "Rower Mat",
        color: "Black",
	model: "TEST TEST TEST", 
      }
	
    }, */
  ];



function sendAlert(text, subject) {
  let transport  = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "rudedogg187@gmail.com",
      pass: "RooCoon517",
    }
  });

  let message = {
    from: 'rudedogg187@gmail.com', 
    to: '5412326031@messaging.sprintpcs.com',        
    subject: subject,
    text: text
  };

  transport.sendMail(message, function(err, info) {
    if (err) {
      //console.log(err)
    } else {
      //console.log(info);
    }
  });
}

function readLog() {
  let log = fs.readFileSync(log_file, "utf8");
  return log;

}

function writeLog(log) {
  fs.writeFileSync(log_file, log)
}


async function main() {
  let log = {
    checks: [],
    alerts: [],
    errors: [],
    beats: [],
  }
  writeLog(JSON.stringify(log, null, 2));

  let i = 0;
  while(true) {
    log = JSON.parse(readLog());
    console.log(JSON.stringify(log, null, 2));


    let now = new Date();
    let hour = now.getHours();
    let now_str = `${now.toLocaleDateString("en-CA")}T${`0${hour}:00:00`.slice(-8)}`
    console.log(now_str)
    if(([7, 19].includes(hour) && !log.beats.includes(now_str)) || !log.beats.length) {
      let subject = `** HEART BEAT ${log.beats.length + 1} **`;
      let text = `${[...Array(50)].join("-")}\n${now.toLocaleString()}\nChecks: ${log.checks.length}\nAlerts: ${log.alerts.length}\nErrors: ${log.errors.length}`

      sendAlert(text, subject);
      log.beats.push(now_str)
      
    }


    let p = path_lst[i];
    let tgt = `${url}/${p.path}`
    let res = await checkConcept2(tgt);
    let descript = `${p.product.color} Model-${p.product.model}`

    if(res.alert) {
      let subject = "** ROUGE ALERT **";
      let text = `${[...Array(50)].join("-")}\n${descript}:\n${tgt}`
      sendAlert(text, subject);
      log.alerts.push({...{dt: res.dt}, ...p.product})
    }

    if(res.error) {
      let subject = "** ROUGE ERROR **";
      let text = `${[...Array(50)].join("-")}\nIssue retrieving:\n${tgt}`
      sendAlert(text, subject);
      log.errors.push({...{dt: res.dt}, ...p.product})

    }


    log.checks.push({...{dt: res.dt}, ...p.product})
    writeLog(JSON.stringify(log, null, 2));
    console.log(JSON.stringify(log, null, 2))

    await delay(1000 * 60 * refresh_minutes)
    i = i >= path_lst.length - 1 ? 0 : i + 1;
  }
}

async function checkConcept2(tgt) {
  let res = await fetch(tgt)
  let txt = await res.text();
  txt = txt.toUpperCase()
   .replace("/* ADD TO CART A/B TEST IE9 FIX */", "/* XXX XX XXX XXX */");

    
  let alert = txt.indexOf("ADD TO CART") >= 0 ? true : false;
  let error = txt.indexOf("CONCEPT") >= 0 ? false : true;
  return { error: error, alert: alert, dt: now() };

}

main()

